# Copyright Russell J. Hewett, 2021.
# Not for distributiuon outside of CMDA 3634 FA 2021.

import numpy as np

# Define directional gliders.
# General Pattern Format: A list, where the first entry is the size of the pattern and the remaining points start out living.
se_glider = [(3,3), (0,1), (1,2), (2,0), (2,1), (2,2)]
ne_glider = [(3,3), (0,0), (0,1), (0,2), (1,2), (2,1)]
sw_glider = [(3,3), (0,1), (1,0), (2,0), (2,1), (2,2)]
nw_glider = [(3,3), (0,0), (0,1), (0,2), (1,0), (2,1)]

# A pattern list is a list of triples of the form (pattern, offset into grid)
pattern_list = [
    (se_glider, (0, 30)),
    (se_glider, (15, 15)),
    (se_glider, (30, 0)),
]


def create_pattern(pattern):
    """
    Assembles a pattern defined using the above format.

    Parameters
    ----------
    pattern : list
        An array size, followed by list of living points.

    Returns
    -------
    An array with the pattern initialized.

    """

    sh = pattern[0]
    pattern = pattern[1:]
    g = np.zeros((sh[0], sh[1]))

    for p in pattern:
        g[p[0], p[1]] = 1

    return g

def build_scene(nr, nc, pattern_list):
    """
    Assembles an nr x nc Life scene based on the preset patterns.

    Note
    ----
    Students shouldn't change this function.

    Parameters
    ----------
    nr : integer
        Number of rows in the grid.
    nc : integer
        Number of columns in the grid.
    pattern_list : list
        A list of triples of the form (pattern, offset, rgb color).

    Returns
    -------
    An array with the scene initialized.

    """

    g = np.zeros((nr, nc))

    for p in pattern_list:
        pattern, origin = p
        _g = create_pattern(pattern)
        sh = _g.shape
        sl = (slice(origin[0], origin[0]+sh[0], 1),
              slice(origin[1], origin[1]+sh[1], 1))
        g[sl] = _g

    return g


def build_glider_train(n, offsets, shifts):
    """
    Assembles a list of patterns to make a glider train, with
    directional gliders coming from each corner.

    Note
    ----
    Students can use this function as inspiration.

    Parameters
    ----------
    n : integer
        Size of grid: nr = nc = n
    offsets : list
        List of offset positions into the grid.  All gliders will start this offset from their corner.
    shifts : list
        List of shifts from the diagonal.  All gliders will rotate this many cells from their diagona.

    """

    pattern_list = list()

    for offset, shift in zip(offsets, shifts):
        right_offset = n-3-offset
        pattern_list += [
                (se_glider, (offset+shift, offset)),
                (ne_glider, (right_offset, offset+shift)),
                (nw_glider, (right_offset-shift, right_offset)),
                (sw_glider, (offset, right_offset-shift)),
            ]
    return pattern_list