#include <stdlib.h>
#include <stdio.h>

int main(int* argc, char** argv){

    int n_data = atoi(argv[1]);
    int* data = malloc(n_data*sizeof(int));
    FILE* f;

    f = fopen(argv[2], "rb");
    int n_read_data = fread(data, sizeof(int), n_data, f);

    printf("Read %d data!\n", n_read_data);

    for(int i=0; i<n_data; i++){
        printf("%d ", data[i]);
    }

    free(data);
    fclose(f);
}
