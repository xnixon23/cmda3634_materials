#include <stdio.h>

// A ...less boring program?

int main(int argc, char** argv){

    if (argc == 1){
        printf("Hello, world!");
    }
    else if (argc == 2){
        printf("Hello, %s!", argv[1]);
    }

    return 0;
}