#include <stdio.h>

int main(){
    // Preferred way
    char* string1 = "The Text in this string is a string literal.";

    // OK, but see next example
    char string2[] = "The Text in this string is a string literal.";
}