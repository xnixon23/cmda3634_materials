#include <stdio.h>
#include <stdlib.h>

// An ... even less boring program?
int main(int argc, char** argv){

    if (argc != 2) {
        printf("Not enough arguments!");
        return 1;
    }

    int n = atoi(argv[1]);

    // Fails:
    // printf("You asked me to print the number %d!", argv[1]);

    // Correct
    printf("You asked me to print the number %d!", n);

    return 0;
}
