#include <stdio.h>
int main(){
    // Preferred way if string variable is re-used
    char* string1 = "The Text in this string is a string literal.";
    // Preferred way if string variable is *not* re-used
    char string2[] = "The Text in this string is a string literal.";

    // A) With a pointer we can do this:
    string1 = "A new string.";

    // B) With array syntax, we _cannot_ do this:
    string2 = "Another new string.";

    // C) With both, this is is OK:
    string1[2] = 'c';
    string2[2] = 'c';
}